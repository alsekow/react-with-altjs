import React from 'react';
import connectToStores from 'alt/utils/connectToStores';

import GridStore from '../stores/GridStore.js';
import GridActions from '../actions/GridActions.js';
import GridCell from './GridCell.jsx';


class Grid extends React.Component {
    constructor() {
        super();
        this.state = GridStore.getState();
        this.render = this.render.bind(this);
        this.renderCell = this.renderCell.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    componentDidMount() {
        GridStore.listen(this.onChange);
        GridActions.loadList();
    }

    componentWillUnmount() {
        GridStore.unlisten(this.onChange);
    }

    onChange(data) {
        this.setState({
            list: data.list
        });
    }

    toggleCell(item) {
        GridActions.toggleItem(item);
    }

    renderCell(item, i) {
        return (
            <GridCell
                data={item}
                onClick={this.toggleCell.bind(this, item)}
                key={i} />
        )
    }

    render() {
        return (
            <div>
        {this.state.list && this.state.list.length > 0 ? this.state.list.map(this.renderCell) : 'Loading...'}
            </div>
        );
    }

}

export default Grid;
