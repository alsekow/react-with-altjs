import React from 'react';


class GridCell extends React.Component {
    render() {
        return (
            <div className="cell">
                <img src={this.props.data.media.m}
                    alt="this.props.data.title"
                    onClick={this.props.onClick}
                    className={this.props.data.toggled ? 'selected' : ''}/>
            </div>
        );
    }
}



export default GridCell;
