import alt from '../alt';
import merge from 'object-assign';

import GridActions from '../actions/GridActions';
import apiCall from '../sources/GridSource';
import LocalStorage from '../sources/LocalStorage';


class GridStore {
    constructor() {
        this.bindListeners({
            loadList: GridActions.loadList,
            toggleItem: GridActions.toggleItem
        });

        this.state = {
            list: []
        }
    }

    loadList() {
        let that = this;
        apiCall().then(data => {
            this.setState({
                list: this.checkSavedFlags(data.items)
            });
        });
    }

    toggleItem(item) {
        this.state.list.map((d, i) => {
            if (d.link === item.link) {
                if (d.toggled)
                    d.toggled = !d.toggled;
                else
                    d.toggled = true;
            }
        });
        this.saveInLocalStorage();
    }

    checkSavedFlags(remoteList) {
        let localList = this.readFromLocalStorage().list;
        if (localList && localList.length > 0) {
            remoteList.map((k, n) => {
                let match = localList.filter(function(d, i) {
                    return d.link == k.link;
                });
                if (match.length > 0 && match[0].hasOwnProperty('toggled'))
                    k.toggled = match[0].toggled;
            });
        }
        return remoteList;
    }

    saveInLocalStorage() {
        LocalStorage.set('gridlist', this.state);
    }

    readFromLocalStorage() {
        return LocalStorage.get('gridlist');
    }

}

export default alt.createStore(GridStore, 'GridStore');
