class LocalStorage {
    constructor() {}
    get(item) {
        if (localStorage)
            return localStorage[item] ? JSON.parse(localStorage[item]) : [];
        else
            return [];
    }
    set(item, value) {
        if (localStorage)
            localStorage[item] = JSON.stringify(value);
    }
};


export default new LocalStorage();