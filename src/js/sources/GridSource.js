import jsonp from 'jsonp';
import Q from 'q';

var apiCall = function() {
    let deferred = Q.defer();
    let tags = 'london';

    jsonp(
        'https://api.flickr.com/services/feeds/photos_public.gne?format=json&tags=' + tags,
        {
            name: 'jsonFlickrFeed'
        },
        function(err, data) {
            deferred.resolve(data);
        }
    );

    return deferred.promise;
};


export default apiCall;