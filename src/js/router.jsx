import { render } from 'react-dom';
import React from 'react';
import {Router, Route, NotFoundRoute, DefaultRoute} from 'react-router';

import Home from './components/Home.jsx';

function AppRouter() {
    render((
        <Router>
            <Route path="/" component={Home}>
            </Route>
        </Router>
    ), document.getElementById('app-wrapper'));
}


export default AppRouter