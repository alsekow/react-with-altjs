import alt from '../alt.js';

class GridActions {
    constructor() {
        this.generateActions(
            'loadList'
        )
    }
    toggleItem(item) {
        this.dispatch(item);
    }
}


export default alt.createActions(GridActions);
